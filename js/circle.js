var breadcrumbWidth = 1200;
var lastcrumbWidth = 350;

// Breadcrumb dimensions: width, height, spacing, width of tip/tail.
var b = {
    w: 75, h: 30, s: 3, t: 10
};


function initializeBreadcrumbTrail() {
    // Add the svg area.
    var trail = d3.select("#sequence").append("svg:svg")
        .attr("width", breadcrumbWidth)
        .attr("height", 50)
        .attr("id", "trail");
}

// Update the breadcrumb trail to show the current sequence and percentage.
function updateBreadcrumbs(nodeArray) {
    // Data join; key function combines name and depth (= position in sequence).
    var g = d3.select("#trail")
        .selectAll("g")
        .data(nodeArray, function (d) {
            return d.data.name + d.depth;
        });

    // Add breadcrumb and label for entering nodes.
    var entering = g.enter().append("svg:g");

    entering.append("svg:polygon")
        .attr("points", breadcrumbPoints)
        .style("fill", function (d) {
            // return colors[d.name];
            return myCustomColor(d);
        });

    entering.append("svg:text")
        .attr("x", function (d) {
            if (!d.children || d.children.length == 0) {
                return (lastcrumbWidth + b.t) / 2;
            } else {
                return (b.w + b.t) / 2;
            }
        })
        .attr("y", b.h / 2)
        .attr("dy", "0.35em")
        .attr("text-anchor", "middle")
        .attr('class', 'crumb')
        .text(function (d) {
            return d.data.name;
        });

    // Remove exiting nodes.
    g.exit().remove();

    // Set position for entering and updating nodes.
    var g = d3.select("#trail")
        .selectAll("g");
    g.attr("transform", function (d, i) {
        return "translate(" + i * (b.w + b.s) + ", 0)";
    });

    // Make the breadcrumb trail visible, if it's hidden.
    d3.select("#trail")
        .style("visibility", "");
}


var svg = d3.select("svg"),
    margin = 20,
    diameter = +svg.attr("width"),
    g = svg.append("g").attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

var color = d3.scaleLinear()
    .domain([-1, 5])
    .range(["hsl(152,80%,80%)", "hsl(228,30%,40%)"])
    .interpolate(d3.interpolateHcl);

var pack = d3.pack()
    .size([diameter - margin, diameter - margin])
    .padding(2);

var myCustomColor = function (d) {
    var fqname = getAncestors(d).reduce(function (acc, val) {
        return acc + val.data.name.replace('-', '');
    }, '');
    return colors[clusterObj[fqname]];
};
var colors;

var colorByAbsoluteName = function (d) {
    var arr = [];
    arr.push(d.data.name.replace('-', ''));
    while (d.parent) {
        d = d.parent;
        arr.push(d.data.name.replace('-', ''));
    }
    var str = arr.reverse().join('');
//        console.log("Color by absolute name : " + str + "   " + clusterObj[str]);
    return colors[clusterObj[str]];
};

var promiseOne = $.getJSON("sunburst.json", function (clusterObj) {
    window.clusterObj = clusterObj;
});

var promiseTwo = $.getJSON("sunburst_cluster_color.json", function (obj) {
    "use strict";
    window.clusterColorJSON = obj;
    colors = obj;
});


$.when([promiseOne, promiseTwo]).then(function () {

    d3.json("circle.json", function (error, root) {
        if (error) throw error;

        initializeBreadcrumbTrail();
        drawLegend();

        root = d3.hierarchy(root)
            .sum(function (d) {
                return d.size || 1;
            })
            .sort(function (a, b) {
                return b.value - a.value;
            });

        var focus = root,
            nodes = pack(root).descendants(),
            view;

        var circle = g.selectAll("circle")
            .data(nodes)
            .enter().append("circle")
            .attr("class", function (d) {
                return d.parent ? d.children ? "node" : "node node--leaf" : "node node--root";
            })
            .style("fill", function (d) {
                return d.children ? color(d.depth) : colorByAbsoluteName(d);
            })
            .on('mouseover', function (d) {
                "use strict";
                // console.log(d.data.name);
                var sequenceArray = getAncestors(d);
                updateBreadcrumbs(sequenceArray);
            })
            .on("click", function (d) {
                // if (d3.select(this).classed("node--leaf")) {
                //     d3.event.stopPropagation();
                //     alert("Component Name : " + d.data.name);
                // } else {
                if (focus !== d) zoom(d), d3.event.stopPropagation();
                // }
            });

        var text = g.selectAll("text:not(.crumb)")
            .data(nodes)
            .enter().append("text")
            .attr("class", "label")
            .style("fill-opacity", function (d) {
                return d.parent === root ? 1 : 0;
            })
            .style("display", function (d) {
                return d.parent === root ? "inline" : "none";
            })
            .text(function (d) {
                return d.data.name;
            });

        var node = g.selectAll("circle,text");

        svg
            .style("background", color(-1))
            .on("click", function () {
                zoom(root);
            });

        zoomTo([root.x, root.y, root.r * 2 + margin]);

        function zoom(d) {
            var focus0 = focus;
            focus = d;

            var transition = d3.transition()
                .duration(d3.event.altKey ? 7500 : 750)
                .tween("zoom", function (d) {
                    var i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2 + margin]);
                    return function (t) {
                        zoomTo(i(t));
                    };
                });

            transition.selectAll("text:not(.crumb)")
                .filter(function (d) {
                    return d.parent === focus || this.style.display === "inline";
                })
                .style("fill-opacity", function (d) {
                    return d.parent === focus ? 1 : 0;
                })
                .on("start", function (d) {
                    if (d.parent === focus) this.style.display = "inline";
                })
                .on("end", function (d) {
                    if (d.parent !== focus) this.style.display = "none";
                });
        }

        function zoomTo(v) {
            var k = diameter / v[2];
            view = v;
            node.attr("transform", function (d) {
                return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")";
            });
            circle.attr("r", function (d) {
                return d.r * k;
            });
        }


        function init(d) {
            var transition = d3.transition()
            transition.selectAll("text")
                .each("start", function (d) {
                    if (d.parent === focus) this.style.display = "inline";
                });
        }

//            init(root)
    });
});