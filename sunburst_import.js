'use strict'
const os = require("os");
const readline = require('readline');
const fs = require('fs');
const q = require('q');

function readLegendDot(legendDotFile) {
    var defer = q.defer();
    var rl = readline.createInterface({
        input: fs.createReadStream(legendDotFile)
    });
    var regex = new RegExp(/^(.+)\[fontcolor\=\"(.+)\"\];$/);
    var colors = {};
    rl.on('line', (line) => {
        if (line.match(regex)) {
            var o = regex.exec(line);
            colors[o[1]] = o[2] == 'black' ? '#000000' : o[2];
        }
    }).on('close', () => {
        defer.resolve(colors);
    });
    return defer.promise;
}

function readClusters(clusterFile) {
    var defer = q.defer();
    var rl = readline.createInterface({
        input: fs.createReadStream(clusterFile)
    });
    var regex = new RegExp("contain (.+) (.+)");
    var arr = [];
    rl.on('line', (line) => {
        var o = regex.exec(line);
        var component = o[2].replace(/\$.+/, '').replace('-', '').replace(/\./g, '-') + ",1";
        arr.push(component)
    }).on('close', () => {
        defer.resolve({
            arr: arr
        });
    });
    return defer.promise;
}

function createColorTree(cluster, obj, prefix) {
    if (!obj.children || obj.children.length == 0) {
        prefix += obj.name;
        obj.cluster = cluster[prefix];
        return;
    }
    for (let index in obj.children) {
        createColorTree(cluster, obj.children[index], prefix + obj.name);
    }
    var temp = {};
    for (let index in obj.children) {
        temp[obj.children[index].cluster] = (temp[obj.children[index].cluster] + 1) || 1;
    }
    let maxClusterCount = 0, maxCluster = undefined;
    for (let c in temp) {
        if (temp[c] > maxClusterCount) {
            maxClusterCount = temp[c];
            maxCluster = c;
        }
    }
    obj.cluster = maxCluster;
}

function colorMapToFile(out, obj, prefix) {
    out[prefix + obj.name.replace('-', '')] = obj.cluster;
    for (let i in obj.children) {
        colorMapToFile(out, obj.children[i], prefix + obj.name);
    }
}

function createFileCSV(obj) {
    let file = fs.createWriteStream('sunburst.csv');
    file.on('error', function () {
        console.log("Something went wrong, please look into appropriate permission.");
    });
    obj.arr.forEach(function (v) {
        file.write(v + '\n');
    });
    file.end();
}

function start(clusterFile, legendDotFile, inputJson, clusterMap) {

    createColorTree(clusterMap, inputJson, "");
    // console.log(JSON.stringify(inputJson));

    readClusters(clusterFile).then(function (obj) {
        createFileCSV(obj);

        let compClusterObj = {};
        colorMapToFile(compClusterObj, inputJson, "");
        // console.log(JSON.stringify(compClusterObj));
        fs.writeFileSync('sunburst.json', JSON.stringify(compClusterObj), 'utf8');
    });

    readLegendDot(legendDotFile).then(function (o) {
        // console.log(JSON.stringify(o));
        fs.writeFileSync('sunburst_cluster_color.json', JSON.stringify(o), 'utf8');
    });
}

module.exports = start;
