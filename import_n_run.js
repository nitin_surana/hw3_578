'use strict'

var args = process.argv.slice(2);

var clusterFile = args[0];      //'apache-log4j-2.3_relax_clusters.rsf';
var dotFile = args[1];          //apache-log4j-2.7_legend.dot
if (!clusterFile) {
    console.log("Please mention the path to cluster file (...relax_clusters.rsf) and dot file (..._legend.dot)");
    process.exit(1);
}

const readline = require('readline');
const fs = require('fs');
const q = require('q');
const sunburst = require('./sunburst_import');

function readClusters() {
    var defer = q.defer();
    var rl = readline.createInterface({
        input: fs.createReadStream(clusterFile)
    });
    var regex = new RegExp("contain (.+) (.+)");
    var obj = {}, cObj = {};
    rl.on('line', (line) => {
        var o = regex.exec(line);
        var component = o[2].replace(/\$.+/, '');
        treeTraverse(obj, component);
        // console.log(obj);
        var cluster = o[1];
        cObj[component.replace(/\./g, '')] = cluster;
        cluster: cObj
    }).on('close', () => {
        defer.resolve({
            tree: obj,
            cluster: cObj
        });
    });
    return defer.promise;
}

function treeTraverse(obj, str) {
    var i = str.indexOf('.');
    if (i >= 0) {
        var key = str.substring(0, i);
        obj[key] = obj[key] || {};
        treeTraverse(obj[key], str.substring(i + 1));
    } else {
        obj[str] = {};
    }
}

function createInputForCircleGraph(result, obj) {
    if (typeof obj == 'string') {
        result.name = obj;
    } else {
        for (var k in obj) {
            var o = {name: k, children: []};
            result.children.push(o);
            createInputForCircleGraph(o, obj[k]);
        }
    }
    return result;
}

function createFiles(temp) {
    var r = temp.tree;
    var clusterObj = temp.cluster;
    // console.log(JSON.stringify(r));
    var result = {name: '', children: []};
    createInputForCircleGraph(result, r);
    // console.log(JSON.stringify(result));
    fs.writeFileSync('circle.json', JSON.stringify(result), 'utf8');
    fs.writeFileSync('cluster.json', JSON.stringify(clusterObj), 'utf8');
    return {
        inputJson: result,
        clusterMap: clusterObj
    };
}

function startServer() {
    var connect = require('connect');
    var serveStatic = require('serve-static');
    connect().use(serveStatic(__dirname)).listen(8080, function () {
        console.log('Server running on 8080...');
    });
}


readClusters().then(function (temp) {
    var o = createFiles(temp);
    sunburst(clusterFile, dotFile, o.inputJson, o.clusterMap);
    startServer();
});